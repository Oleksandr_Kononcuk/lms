from tokenize import group

from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args


from students.utils import format_records
from groups.models import Group

def hello(request):
    return HttpResponse("Hello World!")


@use_args(
    {
        'first_name': fields.Str(
            required=False
        ),
        'search_text': fields.Str(
            required=False
        )
    },
    location='query'
)
def get_groups(request, parameters):
    form = """
       <form>  
          <label>First name:</label><br>
          <input type="text" name="first_name"><br>

          <label>Search :</label><br>
          <input type="text" name="search_text" placeholder="Enter text to search"><br><br>

          <input type="submit" value="Submit"/>        
        </form>     
    """

    groups = Group.objects.all()

    search_fields = ['first_name', 'email']

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == 'search_text':
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__contains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})
    result = format_records

    response = form + result
    return HttpResponse(result)


@csrf_exempt
def update_groups(request, pk):

    student = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('group:get_groups'))

        elif request.method == "GET":
            form = GroupForm(instance=group)

        form_html = f"""
            <form method="POST">
                  {form.as_p()}
                <p><input type="submit" value="Update"/></p>
            </form>
"""
    return HttpResponse("form_html")