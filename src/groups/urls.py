from django.urls import path
from students.views import hello, get_groups, create_group

urlpatterns = [
    path('hello/', hello, ),
    path('', get_groups, ),
    path('create/', create_group, name="create_group"),
    path('update/<int:pk>/', update_group, name="update_group"),
]
