import random

from django.db import models
from faker import Faker


class Groups(models.Model):
    first_name = models.CharField(max_length=128, null=True)
    last_name = models.CharField(max_length=128, null=True)
    email = models.EmailField(max_length=128)
    grate = models.SmallIntegerField(default=0, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.id}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                grade=random.randint(0, 100)
            )
