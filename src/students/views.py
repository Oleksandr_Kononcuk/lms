from audioop import reverse

import Poll as Poll
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from django.http import Http404
from students.forms import StudentForm
from students.models import Student
from students.utils import format_records


def index(request):
    return render(
        request=request,
        template_name="index.html"
    )

def hello(request):
    return HttpResponse("Hello World!")

@use_args(
    {
        'first_name': fields.Str(
            required=False
        ),
        'search_text': fields.Str(
            required=False
        )
    },
    location='query'
)
def get_students(request, parameters):
    form = """
      <form>
         <label>First name:</label><br>
         <input type="text" name="first_name"><br>
        
          <label>Search :</label><br>
          <input type="text" name="search_text" placeholder="Enter text to search"><br><br>
        
          <input type="submit" value="Submit"/>
        </form>
    """

    students = Student.objects.all()

    search_fields = ['first_name', 'last_name', 'email']

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == 'search_text':
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__contains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})
    result = format_records(students)

    response = form + result
    return render(
        request=request,
        template_name="students_list.html",
        context={
            "students": students
        }
    )

@csrf_exempt
def update_student(request, pk):

    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('students:get_students'))

        elif request.method == "GET":
            form = StudentForm(instance=student)

        form_html = f"""
            <form method="POST">
                  {form.as_p()}
                <p><input type="submit" value="Update"/></p>
            </form>
"""
    return HttpResponse("form_html")

def delete_student(request, pk):
    student = get_object_or_404(Student, pk=pk)
    student.delete()

    return HttpResponseRedirect(reverse('students:get_students'))


def detail(request, poll_id):
    try:
        poll = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        raise Http404
    return render(request, 'polls/detail.html', {'poll': poll})
