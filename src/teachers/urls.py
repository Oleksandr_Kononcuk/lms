from django.urls import path
from students.views import hello, get_teachers, create_teachers
from teachers.views import update_teachers

urlpatterns = [
    path('hello/', hello, ),
    path('', get_teachers, ),
    path('create/', create_teachers, name="create_teacher"),
    path('update/<int:pk>/', update_teachers, name="update_teacher"),
]
