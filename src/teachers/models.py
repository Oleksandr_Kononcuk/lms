import datetime
import random

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from students.validators import first_name_validator


class Teachers(models.Model):
    first_name = models.CharField(max_length=128, null=True, validators=[
        MinLengthValidator(2), first_name_validator
    ])
    last_name = models.CharField(max_length=128, null=True, validators=[
        MinLengthValidator(2)
    ])
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.id}"

    def age(self):
        return datetime.datetime.now().year - self.birthdate.year

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(
                    start_data="-80y", end_date="-18y"
                ),
                grade=random.randint(0, 100)
            )
