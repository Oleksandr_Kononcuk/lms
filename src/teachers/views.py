from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args


from students.utils import format_records
from teachers.models import Teachers


def hello(request):
    return HttpResponse("Hello World!")


@use_args(
    {
        'first_name': fields.Str(
            required=False
        ),
        'search_text': fields.Str(
            required=False
        )
    },
    location='query'
)
def get_teachers(request, parameters):
    form = """
       <form>  
          <label>First name:</label><br>
          <input type="text" name="first_name"><br>

          <label>Search :</label><br>
          <input type="text" name="search_text" placeholder="Enter text to search"><br><br>

          <input type="submit" value="Submit"/>        
        </form>     
    """

    teachers = Teachers.objects.all()

    search_fields = ['first_name', 'last_name', 'email']

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == 'search_text':
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__contains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})
    result = format_records

    response = form + result
    return HttpResponse(result)


@csrf_exempt
def update_teachers(request, pk):

    teachers = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=Teacher)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:get_teachers'))

        elif request.method == "GET":
            form = TeacherForm(instance=teacher)

        form_html = f"""
            <form method="POST">
                  {form.as_p()}
                <p><input type="submit" value="Update"/></p>
            </form>
"""
    return HttpResponse("form_html")
